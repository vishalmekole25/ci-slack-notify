# ci-slack-notify

Advanced Slack notifications for Gitlab CI based on CloudPosse's [`slack-notifier` golang utility](https://github.com/cloudposse/slack-notifier) and it's [Docker container](https://hub.docker.com/r/cloudposse/slack-notifier).

Utilizes a Slack Incoming webhook URL passed as an environment variable - look into `.gitlab-ci.yml` for an example. You can include Slack job template directly in your project using [remote include](https://docs.gitlab.com/ee/ci/yaml/includes.html#single-string-or-array-of-multiple-values):
```yaml
image: alpine
include:
    - remote: 'https://gitlab.com/ovivtash/ci-slack-notify/-/raw/master/.gitlab-ci-slack.yml'

myjob:
    script: echo -e "This is my job"
```

For fine-tuning your message, refer to the utility [env variable reference](https://github.com/cloudposse/slack-notifier#usage).

Message example:

![sample](/msg-sample.png)